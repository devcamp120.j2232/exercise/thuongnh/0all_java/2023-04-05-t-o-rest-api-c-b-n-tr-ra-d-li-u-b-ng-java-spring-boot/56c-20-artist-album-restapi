package com.devcamp.artistalbumapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.artistalbumapi.model.Artist;


@Service
public class ArtistService {
        @Autowired
        private AlbumService albumService;

        Artist ngheSi1 = new Artist(1, "thuong");
        Artist ngheSi2 = new Artist(2, "duong");
        Artist ngheSi3 = new Artist(3, "tram");

        // phương thức trả về tất cả các nghệ sỹ 
        public ArrayList<Artist> getAllArt(){
            // set Art 
            ngheSi1.setAlbums(albumService.getArtThuong());
            ngheSi2.setAlbums(albumService.getArtHuy());
            ngheSi3.setAlbums(albumService.getArtTram());
            // khởi tạo 1 đối tượng array list 
            ArrayList<Artist> artLists = new ArrayList<Artist>();
            // thêm các nghệ sĩ vào ARRLIsT
            artLists.add(ngheSi1);
            artLists.add(ngheSi2);
            artLists.add(ngheSi3);
            // trả về Arrlist các nghệ sỹ 
            return  artLists;   
             
        }


        // phương  thức lấy info của nghệ sỹ 
        public Artist getInfoArtict(int paramId){
            // tất cả các nghệ sỹ
             // set Art 
             ngheSi1.setAlbums(albumService.getArtThuong());
             ngheSi2.setAlbums(albumService.getArtHuy());
             ngheSi3.setAlbums(albumService.getArtTram());
             // khởi tạo 1 đối tượng array list 
             ArrayList<Artist> artLists = new ArrayList<Artist>();
             // thêm các nghệ sĩ vào ARRLIsT
             artLists.add(ngheSi1);
             artLists.add(ngheSi2);
             artLists.add(ngheSi3);
           // ArrayList<Artist> allArtist = ArtistService.getAllArt();
            // khởi tạo 1 đối tượng để lưu kết quả
            Artist infoArtist = new Artist();
            // lọc nghệ sỹ theo id 
            for(Artist artistElement : artLists){
                if(artistElement.getId() ==paramId){
                    infoArtist = artistElement;
                }
            }

            return infoArtist;
        }
        

}
