package com.devcamp.artistalbumapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.artistalbumapi.model.Album;
import com.devcamp.artistalbumapi.service.AlbumService;

@CrossOrigin
@RestController
public class AlbumController {
    @Autowired
    private AlbumService album1Service;
    @GetMapping("album-info")
    public Album getAlbumById(@RequestParam(name="id") int albumId){
        Album   album = album1Service.getFilterAlbumAll(albumId);
        return album;

    }
    
}
